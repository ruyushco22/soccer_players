<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<main id="main" class="main">
	<div class="pagetitle">
		<h1>Position</h1>
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
				<li class="breadcrumb-item">Position</li>
			</ol>
		</nav>
	</div>
	<?php if ($this->session->flashdata('confirmacion')): ?>
		<div id="alerta-success" class="alert alert-success alert-dismissible fade show" role="alert">
			<?php echo $this->session->flashdata('confirmacion'); ?>
			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>
		<?php $this->session->set_flashdata('confirmacion',''); ?>
	<?php endif; ?>

	<script type="text/javascript">
		setTimeout(function() {
			document.getElementById('alerta-success').classList.remove('show');
		}, 3000); 
	</script>

	
	<section class="section">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">Position registration</h5>
						<!-- Multi Columns Form -->
						<form class="row g-3" method="POST" action="<?php echo site_url('posiciones/guardarPosicion') ?>" enctype="multipart/form-data" id="formPosicion">

							<div class="col-md-6">
								<label class="form-label" for="nombre_pos"><b>Position name:</b></label>
								<input type="text" class="form-control" id="nombre_pos" name="nombre_pos" required />
							</div>
							<div class="col-md-6">
								<label class="form-label" for="descripcion_pos"><b>Description:</b></label>
								<input type="text" class="form-control" id="descripcion_pos" name="descripcion_pos" required />
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-outline-success">
								 	Register <i class="bx bx-save"></i>
								</button>
								<button type="reset" class="btn btn-outline-danger">
									Cancel <i class="bx bx-message-square-x"></i>
								</button>
							</div>
						</form>
						<!-- End Multi Columns Form -->
					</div>
				</div>
			</div>
			
		</div>
		<div class="row">
			<!-- Recent Ventas -->
			<div class="col-12">
				<div class="card recent-sales overflow-auto">
					<div class="card-body">
						<h5 class="card-title">Player list</h5>
						<?php if ($listadoPosicion) : ?>
							<table class="table w-100" id="tableArticulos" style="border-collapse: collapse;">
								<thead>
									<tr>
										<th class="text-center" style="border: 1px solid #ddd;">No</th>
										<th class="text-center" style="border: 1px solid #ddd;">POSITION NAME</th>
										<th class="text-center" style="border: 1px solid #ddd;">DESCRIPTION</th>
										<th class="text-center" style="border: 1px solid #ddd;">ACTIONS</th>
									</tr>
								</thead>
								<tbody>
									<?php $contador = 1; ?>
									<?php foreach ($listadoPosicion as $posicion) : ?>
										<tr>
											<td class="text-center" style="border: 1px solid #ddd;"><?php echo $contador; ?></td>
											<td class="text-center" style="border: 1px solid #ddd;"><?php echo $posicion->nombre_pos; ?></td>
											<td class="text-center" style="border: 1px solid #ddd;"><?php echo $posicion->descripcion_pos; ?></td>
											<td class="text-center" style="border: 1px solid #ddd;">
												<a href="<?php echo site_url('posiciones/borrar/') . $posicion->id_pos; ?>" class=" btn btn-outline-warning btn-eliminar" title="Borrar">
													<i class="bi bi-trash3-fill"></i>
												</a>
												<a href="<?php echo site_url('posiciones/editar/') . $posicion->id_pos; ?>" class=" btn btn-outline-primary" title="Editar">
													<i class="bi bi-pen"></i>
												</a>
											</td>
										</tr>
										<?php $contador++; ?>
									<?php endforeach; ?>
								</tbody>
							</table>
						<?php else : ?>
							<div class="alert alert-danger">
								No se encontro articulos registrados
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		</div>
	</section>
</main>
<script>
	$(document).ready(function() {
	$.validator.addMethod(
		'lettersonly',
		function(value, element) {
			return this.optional(element) || /^[a-zA-Z\s]*$/.test(value)
		},
		'Por favor, ingrese solo letras'
	)

	$('#formPosicion').validate({
		rules: {
			nombre_pos: {
				required: true,
				lettersonly: true,
				maxlength: 30,
				minlength: 3,
			},
			descripcion_pos: {
				required: true,
          		
			},
			
		},
		messages: {
			nombre_pos: {
				required: 'Please enter the position name',
				lettersonly: 'Only letters are allowed in this field',
				maxlength: 'The position must be less than 30 characters',
				minlength: 'The position must have at least 3 characters',

				
			},
			descripcion_pos: {
				required: 'Please enter a description',
			},
			
		},
	})
})
</script>
<!-- End #main -->
<script>
  $(document).ready(function() {
    // Captura el clic en el botón de eliminar
    $('.btn-eliminar').click(function(event) {
      // Previene el comportamiento predeterminado del enlace
      event.preventDefault();
      // Muestra una alerta de confirmación utilizando SweetAlert2
      Swal.fire({
        title: 'Are you sure?',
        text: "This action cannot be undone",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete',
        cancelButtonText: 'Cancel'
      }).then((result) => {
        // Si el usuario confirma, redirige al enlace de eliminación
        if (result.isConfirmed) {
          window.location.href = $(this).attr('href');
        }
      });
    });
  });
</script>