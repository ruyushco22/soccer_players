<main id="main" class="main">
  <div class="pagetitle">
    <h1>Positions</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
        <li class="breadcrumb-item">Edit Positions</li>
      </ol>
    </nav>
  </div>
  <?php if ($this->session->flashdata('confirmacion')): ?>
		<div id="alerta-success" class="alert alert-success alert-dismissible fade show" role="alert">
			<?php echo $this->session->flashdata('confirmacion'); ?>
			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>
		<?php $this->session->set_flashdata('confirmacion',''); ?>
	<?php endif; ?>

	<script type="text/javascript">
		setTimeout(function() {
			document.getElementById('alerta-success').classList.remove('show');
		}, 3000); 
	</script>
  <!-- End Page Title -->
  <section class="section">
    <div class="row">
      <div class="col-md-12"> 
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Edit positions</h5>
            <!-- Multi Columns Form -->
            <form class="row g-3" method="POST" action="<?php echo site_url('posiciones/actualizarPosicion') ?>"
              enctype="multipart/form-data" id="formPosicion">
              <input type="hidden" name="id_pos" id="id_pos" value="<?php echo $posicionEditar->id_pos; ?>">

              <div class="col-md-6">
								<label class="form-label" for="nombre_pos"><b>Position name:</b></label>
								<input type="text" class="form-control" value="<?php echo $posicionEditar->nombre_pos; ?>" id="nombre_pos" name="nombre_pos" required />
							</div>
							<div class="col-md-6">
								<label class="form-label" for="descripcion_pos"><b>Description:</b></label>
								<input type="text" class="form-control" value="<?php echo $posicionEditar->descripcion_pos; ?>" id="descripcion_pos" name="descripcion_pos" required />
							</div>
              
              <div class="text-center">
                <button type="submit" class="btn btn-outline-warning">
                  Update <i class="bx bx-refresh" ></i>
                </button>
                <a href="<?php echo site_url('posiciones/index'); ?>" class="btn btn-outline-danger"> 
                <i class="bx bx-message-square-x"></i> 
                Cancel
                </a>
              </div>
            </form>
            <!-- End Multi Columns Form -->
          </div>
        </div>
      </div>
    </div>
    
  </section>
</main>
<!-- End #main -->
