<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Update player</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap Select CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css" rel="stylesheet">
</head>
<body>
	
</body>
<main id="main" class="main">
	<div class="pagetitle">
		<h1>Player</h1>
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/') ?>">Inicio</a></li>
				<li class="breadcrumb-item">Edit player</li>
			</ol>
		</nav>
	</div>
	<?php if ($this->session->flashdata('confirmacion')): ?>
		<div id="alerta-success" class="alert alert-success alert-dismissible fade show" role="alert">
			<?php echo $this->session->flashdata('confirmacion'); ?>
			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>
		<?php $this->session->set_flashdata('confirmacion',''); ?>
	<?php endif; ?>

	<script type="text/javascript">
		setTimeout(function() {
			document.getElementById('alerta-success').classList.remove('show');
		}, 3000); 
	</script>
	<!-- End Page Title -->
	<section class="section">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">Edit player</h5>
						<!-- Multi Columns Form -->
						<form class="row g-3" method="POST" action="<?php echo site_url('jugadores/actualizarJugador') ?>" enctype="multipart/form-data" id="formJugadores">
							<input type="hidden" name="id_jug" id="id_jug" value="<?php echo $JugadorEditar->id_jug; ?>">
							<div class="col-md-6">
								<label class="form-label" for="apellido_jug"><b>Last name:</b></label>
								<input type="text" class="form-control" value="<?php echo $JugadorEditar->apellido_jug; ?>" id="apellido_jug" name="apellido_jug" />
							</div>

							<div class="col-md-6">
								<label class="form-label" for="nombre_jug"><b>Name:</b></label>
								<input type="text" class="form-control" value="<?php echo $JugadorEditar->nombre_jug; ?>" id="nombre_jug" name="nombre_jug" />
							</div>
							<div class="col-md-6">
								<label for="estatura_jug" class="form-label"><b>Height:</b></label>
								<input type="number" class="form-control" value="<?php echo $JugadorEditar->estatura_jug; ?>" id="estatura_jug" name="estatura_jug"  />
							</div>
							<div class="col-md-6">
								<label for="salario_jug" class="form-label"><b>Salary:</b></label>
								<input type="number" class="form-control" value="<?php echo $JugadorEditar->salario_jug; ?>" id="salario_jug" name="salario_jug" />
							</div>
							
							<div class="col-md-4">
								<label for="estado_jug" class="form-label"><b>Status:</b></label>
								<select name="estado_jug" id="estado_jug" class="form-control selectpicker" required data-live-search="true">
									<option value="Activo" <?php echo ($JugadorEditar->estado_jug == 'Activo') ? 'selected' : ''; ?>>Activo</option>
									<option value="Inactivo" <?php echo ($JugadorEditar->estado_jug == 'Inactivo') ? 'selected' : ''; ?>>Inactivo</option>
								</select>
							</div>

							
							<div class="col-md-4">
							<label for="estado_jug" class="form-label"><b>Position:</b></label>
								<select name="fk_id_pos" id="fk_id_pos" class="form-control selectpicker" required data-live-search="true">
									<option value="">Select a position</option>
									<?php foreach ($listadoPosicion as $posicion): ?>
										<option value="<?php echo $posicion->id_pos; ?>" <?php if ($posicion->id_pos == $JugadorEditar->fk_id_pos) echo 'selected'; ?>>
											<?php echo $posicion->nombre_pos; ?>
										</option>									
									<?php endforeach; ?>
								</select>
							</div>
							<div class="col-md-4" >
								<label for="estado_jug" class="form-label"><b>Team:</b></label>
								<select name="fk_id_equi" id="fk_id_equi" class="form-control selectpicker" required data-live-search="true">
									<option value="">Select a team</option> 
									<?php foreach ($listadoEquipos as $equipo): ?>
										<option value="<?php echo $equipo->id_equi; ?>" <?php if ($equipo->id_equi == $JugadorEditar->fk_id_equi) echo 'selected'; ?>>
											<?php echo $equipo->nombre_equi; ?>
										</option>									
									<?php endforeach; ?>
								</select>
							</div>
							<div class="text-center">
								<button type="submit" class="btn btn-outline-warning">
								Update <i class="bx bx-refresh" ></i>
								</button>

								<a href="<?php echo site_url('jugadores/index'); ?>" class="btn btn-outline-danger">
									<i class="bx bx-message-square-x"></i>
									Cancel
								</a>

							</div>
						</form>
						<!-- End Multi Columns Form -->
					</div>
				</div>
			</div>
		</div>

	</section>
</main>
<!-- End #main -->
 <!-- jQuery -->
 <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
    <!-- Bootstrap Select JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <!-- jQuery Validation Plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <!-- jQuery Validation Additional Methods -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script>
        $(document).ready(function() {
            // Inicializar Bootstrap Select con live search
            $('.selectpicker').selectpicker();

            // Validación del formulario
            $.validator.addMethod(
		'lettersonly',
		function(value, element) {
			return this.optional(element) || /^[a-zA-Z\s]*$/.test(value)
		},
		'Por favor, ingrese solo letras'
	)
	$.validator.addMethod(
			'heightrange',
			function(value, element) {
				// Convertir el valor de altura a número
				var height = parseFloat(value);
				// Validar el rango
				return this.optional(element) || (height >= 1.50 && height <= 2.15);
			},
			'The height must be between 1.50m and 2.15m'
		);
	$.validator.addMethod(
			'salaryrange',
			function(value, element) {
				// Convertir el valor de salario a número
				var salary = parseFloat(value);
				// Validar el rango
				return this.optional(element) || (salary >= 25000 && salary <= 70000);
			},
			'The salary must be between 25,000.00 and 70,000.00.'
		);
	$.validator.addMethod(
        'firstletteruppercase',
        function(value, element) {
            // Verificar si la primera letra es mayúscula
            return /^[A-Z]/.test(value);
        },
        'The first letter must be uppercase'
    );
	$('#formJugadores').validate({
		rules: {
			nombre_jug: {
				required: true,
				lettersonly: true,
				firstletteruppercase: true,
				maxlength: 15,
				minlength: 3,
			},
			apellido_jug: {
				required: true,
				lettersonly: true,
				firstletteruppercase: true,
				maxlength: 15,
				minlength: 3,
			},
			estatura_jug: {
				required: true,
				number: true,
				heightrange: true,
				
			},
			salario_jug: {
				required: true,
				number: true,
				salaryrange: true,
				
			},
			estado_jug: {
				required: true,
				
			},
			fk_id_pos: {
				required: true,
				
			},
			fk_id_equi: {
				required: true,
				
			},
		},
		messages: {
			nombre_jug: {
				required: 'Please enter the name',
				lettersonly: 'Only letters are allowed in this field',
				firstletteruppercase: 'The first letter must be uppercase',
				maxlength: 'The name must be less than 15 characters',
				minlength: 'The name must have at least 3 characters',
			},
			apellido_jug: {
				required: 'Please enter the last name',
				lettersonly: 'Only letters are allowed in this field',
				firstletteruppercase: 'The first letter must be uppercase',
				maxlength: 'The last name must be less than 15 characters',
				minlength: 'The last name must have at least 3 characters',
			},
			estatura_jug: {
				required: 'Please enter a height',
				number: 'Please enter a valid number for height',
				required: 'The height must be between 1.50m and 2.15m',
         		
			},
			salario_jug: {
				required: 'Please enter a valid salary amount',
				number: 'Only numerical values are allowed',
				salaryrange:'The salary must be between 25,000.00 and 70,000.00.',
				
			},
			estado_jug: {
				required: 'Please select a state',
          
			},
			fk_id_pos: {
				required: 'Please select a position',
          		
			},
			fk_id_equi: {
				required: 'Please select a team',
          		
			},
		},
	})
        });
    </script>
</body>
</html>