<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Player Registration Form</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap Select CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css" rel="stylesheet">
</head>
<body>
  <main id="main" class="main">
    <div class="pagetitle">
      <h1>Players</h1>
      <nav>
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="<?php echo site_url('/') ?>">Inicio</a></li>
          <li class="breadcrumb-item">Players</li>
        </ol>
      </nav>
    </div>
    <?php if ($this->session->flashdata('confirmacion')): ?>
      <div id="alerta-success" class="alert alert-success alert-dismissible fade show" role="alert">
        <?php echo $this->session->flashdata('confirmacion'); ?>
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
      </div>
      <?php $this->session->set_flashdata('confirmacion',''); ?>
    <?php endif; ?>
  
    <script type="text/javascript">
      setTimeout(function() {
        document.getElementById('alerta-success').classList.remove('show');
      }, 3000); 
    </script>
    <!-- End Page Title -->
    <section class="section">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Player registration</h5>
              <!-- Multi Columns Form -->
              <form class="row g-3" method="POST" action="<?php echo site_url('jugadores/guardarJugador') ?>" enctype="multipart/form-data" id="formJugadores">
  
                <div class="col-md-6">
                  <label class="form-label" for="apellido_jug"><b>Last name:</b></label>
                  <input type="text" class="form-control" id="apellido_jug" name="apellido_jug" />
                </div>
  
                <div class="col-md-6">
                  <label class="form-label" for="nombre_jug"><b>Name:</b></label>
                  <input type="text" class="form-control" id="nombre_jug" name="nombre_jug" />
                </div>
                <div class="col-md-6">
                  <label for="estatura_jug" class="form-label"><b>Height:</b></label>
                  <input type="number" class="form-control" id="estatura_jug" name="estatura_jug"  />
                </div>
                <div class="col-md-6">
                  <label for="salario_jug" class="form-label"><b>Salary:</b></label>
                  <input type="number" class="form-control" id="salario_jug" name="salario_jug" />
                </div>
                
                <div class="col-md-4">
                  <label for="estado_jug" class="form-label"><b>Status:</b></label>
                  <select name="estado_jug" id="estado_jug" class="form-control selectpicker "data-live-search="true">
                    <option value="">Select a Status</option>
                    <option value="Activo">Active</option>
                    <option value="Inactivo">Inactive</option>
                  </select>
                </div>
                
                <div class="col-md-4">
                  <label for="estado_jug" class="form-label"><b>Position:</b></label>
                  <select name="fk_id_pos" id="fk_id_pos" class="form-control selectpicker" required data-live-search="true">
                    <option value="">Select a position</option>
                    <?php foreach ($listadoPosicion as $posicion): ?>
                      <option value="<?php echo $posicion->id_pos; ?>"><?php echo $posicion->nombre_pos; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-md-4" >
                  <label for="estado_jug" class="form-label"><b>Team:</b></label>
                  <select name="fk_id_equi" id="fk_id_equi" class="form-control selectpicker" required data-live-search="true">
                    <option value="">Select a team</option> 
                    <?php foreach ($listadoEquipos as $equipo): ?>
                      <option value="<?php echo $equipo->id_equi; ?>"><?php echo $equipo->nombre_equi; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                
                <div class="text-center">
                  <button type="submit" class="btn btn-outline-success">
                    Register <i class="bx bx-save"></i>
                  </button>
                  <button type="reset" class="btn btn-outline-danger">
                    Cancel <i class="bx bx-message-square-x"></i>
                  </button>
                </div>
              </form>
              <!-- End Multi Columns Form -->
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <!-- Recent Ventas -->
        <div class="col-12">
          <div class="card recent-sales overflow-auto">
            <div class="card-body">
              <h5 class="card-title">Player list</h5>
              <?php if ($listadoJugadores) : ?>
                <table class="table w-100" id="tableJugadores">
                  <thead>
                    <tr>
                      <th class="text-center" style="border: 1px solid #ddd;">No</th>
                      <th class="text-center" style="border: 1px solid #ddd;">LAST NAME</th>
                      <th class="text-center" style="border: 1px solid #ddd;">NAME</th>
                      <th class="text-center" style="border: 1px solid #ddd;">HEIGHT</th>
                      <th class="text-center" style="border: 1px solid #ddd;">SALARY</th>
                      <th class="text-center" style="border: 1px solid #ddd;">STATUS</th>
                      <th class="text-center" style="border: 1px solid #ddd;">POSITION</th>
                      <th class="text-center" style="border: 1px solid #ddd;">TEAM</th>
                      <th class="text-center" style="border: 1px solid #ddd;">ACTIONS</th>
                    </tr>
                  </thead>
                  <?php
                    usort($listadoJugadores, function($a, $b) {
                      
                      $apellidoComparison = strcmp($a->apellido_jug, $b->apellido_jug);
                      if ($apellidoComparison !== 0) {
                        return $apellidoComparison;
                      }
                      
                      return strcmp($a->nombre_jug, $b->nombre_jug);
                    });
                  ?>
                  <tbody>
                    <?php $contador = 1; ?>
                    <?php foreach ($listadoJugadores as $jugador) : ?>
                      <tr>
                          <td class="text-center" style="border: 1px solid #ddd;"><?php echo $contador; ?></td>
                        <td class="text-center" style="border: 1px solid #ddd;"><?php echo $jugador->apellido_jug; ?></td>
                        <td class="text-center" style="border: 1px solid #ddd;"><?php echo $jugador->nombre_jug; ?></td>
                        <td class="text-center" style="border: 1px solid #ddd;"><?php echo $jugador->estatura_jug; ?></td>
                        <td class="text-center" style="border: 1px solid #ddd;"><?php echo $jugador->salario_jug; ?></td>
                        <td class="text-center" style="border: 1px solid #ddd;"><?php echo $jugador->estado_jug; ?></td>
                        <td class="text-center" style="border: 1px solid #ddd;"><?php echo $jugador->nombre_pos; ?></td>
                        <td class="text-center" style="border: 1px solid #ddd;"><?php echo $jugador->nombre_equi; ?></td>
                        <td class="text-center" style="border: 1px solid #ddd;">
                          <a href="<?php echo site_url('jugadores/borrar/') . $jugador->id_jug; ?>" class=" btn btn-outline-warning btn-eliminar" title="Borrar">
                            <i class="bi bi-trash3-fill"></i>
                          </a>
                          <a href="<?php echo site_url('jugadores/editar/') . $jugador->id_jug; ?>" class=" btn btn-outline-primary" title="Editar">
                            <i class="bi bi-pen"></i>
                          </a>
                        </td>
                      </tr>
                      <?php $contador++; ?>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              <?php else : ?>
                <div class="alert alert-danger">
                  No se encontro autores registrados
                </div>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
      <!-- End Recent Ventas -->
      </div>
    </section>
  </main>

    <!-- jQuery -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
    <!-- Bootstrap Select JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <!-- jQuery Validation Plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <!-- jQuery Validation Additional Methods -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script>
        $(document).ready(function() {
            // Inicializar Bootstrap Select con live search
            $('.selectpicker').selectpicker();

            // Validación del formulario
            $.validator.addMethod(
		'lettersonly',
		function(value, element) {
			return this.optional(element) || /^[a-zA-Z\s]*$/.test(value)
		},
		'Por favor, ingrese solo letras'
	)
	$.validator.addMethod(
			'heightrange',
			function(value, element) {
				// Convertir el valor de altura a número
				var height = parseFloat(value);
				// Validar el rango
				return this.optional(element) || (height >= 1.50 && height <= 2.15);
			},
			'The height must be between 1.50m and 2.15m'
		);
	$.validator.addMethod(
			'salaryrange',
			function(value, element) {
				// Convertir el valor de salario a número
				var salary = parseFloat(value);
				// Validar el rango
				return this.optional(element) || (salary >= 25000 && salary <= 70000);
			},
			'The salary must be between 25,000.00 and 70,000.00.'
		);
	$.validator.addMethod(
        'firstletteruppercase',
        function(value, element) {
            // Verificar si la primera letra es mayúscula
            return /^[A-Z]/.test(value);
        },
        'The first letter must be uppercase'
    );
	$('#formJugadores').validate({
		rules: {
			nombre_jug: {
				required: true,
				lettersonly: true,
				firstletteruppercase: true,
				maxlength: 15,
				minlength: 3,
			},
			apellido_jug: {
				required: true,
				lettersonly: true,
				firstletteruppercase: true,
				maxlength: 15,
				minlength: 3,
			},
			estatura_jug: {
				required: true,
				number: true,
				heightrange: true,
				
			},
			salario_jug: {
				required: true,
				number: true,
				salaryrange: true,
				
			},
			estado_jug: {
				required: true,
				
			},
			fk_id_pos: {
				required: true,
				
			},
			fk_id_equi: {
				required: true,
				
			},
		},
		messages: {
			nombre_jug: {
				required: 'Please enter the name',
				lettersonly: 'Only letters are allowed in this field',
				firstletteruppercase: 'The first letter must be uppercase',
				maxlength: 'The name must be less than 15 characters',
				minlength: 'The name must have at least 3 characters',
			},
			apellido_jug: {
				required: 'Please enter the last name',
				lettersonly: 'Only letters are allowed in this field',
				firstletteruppercase: 'The first letter must be uppercase',
				maxlength: 'The last name must be less than 15 characters',
				minlength: 'The last name must have at least 3 characters',
			},
			estatura_jug: {
				required: 'Please enter a height',
				number: 'Please enter a valid number for height',
				required: 'The height must be between 1.50m and 2.15m',
         		
			},
			salario_jug: {
				required: 'Please enter a valid salary amount',
				number: 'Only numerical values are allowed',
				salaryrange:'The salary must be between 25,000.00 and 70,000.00.',
				
			},
			estado_jug: {
				required: 'Please select a state',
          
			},
			fk_id_pos: {
				required: 'Please select a position',
          		
			},
			fk_id_equi: {
				required: 'Please select a team',
          		
			},
		},
	})
        });
    </script>

<script>
      $(document).ready(function() {
        // Captura el clic en el botón de eliminar
        $('.btn-eliminar').click(function(event) {
          // Previene el comportamiento predeterminado del enlace
          event.preventDefault();
          // Muestra una alerta de confirmación utilizando SweetAlert2
          Swal.fire({
            title: 'Are you sure?',
            text: "This action cannot be undone",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete',
            cancelButtonText: 'Cancel'
          }).then((result) => {
            // Si el usuario confirma, redirige al enlace de eliminación
            if (result.isConfirmed) {
              window.location.href = $(this).attr('href');
            }
          });
        });
      });
    </script>

