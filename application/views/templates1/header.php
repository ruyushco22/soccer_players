<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">
	<title>Soccer Players</title>
	<meta content="" name="description">
	<meta content="" name="keywords">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/jquery.validate.min.js" integrity="sha512-WMEKGZ7L5LWgaPeJtw9MBM4i5w5OSBlSjTjCtSnvFJGSVD26gE5+Td12qN5pvWXhuWaWcVwF++F7aqu9cvqP0A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/additional-methods.min.js" integrity="sha512-TiQST7x/0aMjgVTcep29gi+q5Lk5gVTUPE9XgN0g96rwtjEjLpod4mlBRKWHeBcvGBAEvJBmfDqh2hfMMmg+5A==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.20.0/localization/messages_ar.min.js" integrity="sha512-rLFPpaF3u7n96Yj1paoZ5GBSAGR1ETxKo0T+kD8XHlyj1dDSMBwC7EPavNWpHUTqVKMI2F8yc9mlDSUCWaztRQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

	<!-- Favicons -->
	<link href="<?= base_url('assets/img/favicon.png') ?>" rel="icon">
	<link href="<?= base_url('assets/img/apple-touch-icon.png') ?>" rel="apple-touch-icon">
	<!-- Google Fonts -->
	<link href="https://fonts.gstatic.com" rel="preconnect">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
	<!-- Vendor CSS Files -->
	<link href="<?= base_url('assets/vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/vendor/bootstrap-icons/bootstrap-icons.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendor/boxicons/css/boxicons.min.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendor/quill/quill.snow.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendor/quill/quill.bubble.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendors/select2/select2.min.css') ?>" rel="stylesheet">
	
	
	<link href="<?= base_url('assets/vendor/remixicon/remixicon.css') ?>" rel="stylesheet">
	<link href="<?= base_url('assets/vendor/simple-datatables/style.css') ?>" rel="stylesheet">
	<!-- Template Main CSS File -->
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet">
	<!-- =======================================================
  * Template Name: NiceAdmin
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Updated: Apr 20 2024 with Bootstrap v5.3.3
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
======================================================== -->
</head>
<style>
	label.error {
		color: red;
		font-size: 17px;
	}
</style>

<body>

	<!-- ======= Header ======= -->
	<header id="header" class="header fixed-top d-flex align-items-center">
		<div class="d-flex align-items-center justify-content-between">
			<a href="<?php echo site_url('/') ?>" class="logo d-flex align-items-center">
				<img src="<?= base_url('assets/img/logo.png') ?>" alt="">
				<span class="d-none d-lg-block">Soccer Players</span>
			</a>
			<i class="bi bi-list toggle-sidebar-btn"></i>
		</div><!-- End Logo -->
		<div class="search-bar">
			<form class="search-form d-flex align-items-center" method="POST" action="#">
				<input type="text" name="query" placeholder="Buscar" title="Enter search keyword">
				<button type="submit" title="Search"><i class="bi bi-search"></i></button>
			</form>
		</div><!-- End Search Bar -->
		<nav class="header-nav ms-auto">
			<ul class="d-flex align-items-center">
				<li class="nav-item d-block d-lg-none">
					<a class="nav-link nav-icon search-bar-toggle " href="#">
						<i class="bi bi-search"></i>
					</a>
				</li><!-- End Search Icon-->
				<li class="nav-item dropdown pe-3">
					<a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
						<img src="<?php echo base_url('assets/img/user.png') ?>" alt="Perfil" class="rounded-circle">
						<span class="d-none d-md-block dropdown-toggle ps-2">Eddy Ushco</span>
					</a><!-- End Profile Iamge Icon -->
					<ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
						<li class="dropdown-header">
							<h6>Eddy Ushco</h6>
							<span>Software Developer</span>
						</li>
						<li>
							<hr class="dropdown-divider">
						</li>
						<li>
							<a class="dropdown-item d-flex align-items-center" href="users-profile.html">
								<i class="bi bi-person"></i>
								<span>Mi perfil</span>
							</a>
						</li>
						<li>
							<hr class="dropdown-divider">
						</li>
						<li>
							<a class="dropdown-item d-flex align-items-center" href="users-profile.html">
								<i class="bi bi-gear"></i>
								<span>Configuracion de la cuenta</span>
							</a>
						</li>
						<li>
							<hr class="dropdown-divider">
						</li>
						<li>
							<a class="dropdown-item d-flex align-items-center" href="pages-faq.html">
								<i class="bi bi-question-circle"></i>
								<span>Necesitas ayuda?</span>
							</a>
						</li>
						<li>
							<hr class="dropdown-divider">
						</li>
						<li>
							<a class="dropdown-item d-flex align-items-center" href="#">
								<i class="bi bi-box-arrow-right"></i>
								<span>Cerrar Sesion</span>
							</a>
						</li>
					</ul><!-- End Profile Dropdown Items -->
				</li><!-- End Profile Nav -->
			</ul>
		</nav><!-- End Icons Navigation -->
	</header><!-- End Header -->
	<!-- ======= Sidebar ======= -->
	<aside id="sidebar" class="sidebar">
		<ul class="sidebar-nav" id="sidebar-nav">
			<li class="nav-heading">Menu</li>
			
			
			<!-- AUTOR-->
			<li class="nav-item">
				<a class="nav-link collapsed" href="<?php echo site_url('equipos/index'); ?>">
					<i class="bi bi-person-bounding-box"></i>
					<span>Team</span>
				</a>
			</li>
			<!-- ARTICULO-->
			<li class="nav-item">
				<a class="nav-link collapsed " href="<?php echo site_url('posiciones/index'); ?>">
				<i class="bi bi-shield-check"></i>
					<span>Positions</span>
				</a>
			</li>
			<!-- ARBITRAJE-->
			<li class="nav-item">
				<a class="nav-link collapsed " href="<?php echo site_url('jugadores/index'); ?>">
				<i class="bi bi-person"></i>
					<span>Players</span>
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link collapsed " href="<?php echo site_url('jugadores/grafico'); ?>">
					<i class="bi bi-clock-history"></i>
					<span>Graficos</span>
				</a>
			</li>
			


			</li>
			<!-- INDEXACION-->
			

			
			
			
		</ul>
	</aside><!-- End Sidebar-->
