<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Player Registration Form</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap Select CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css" rel="stylesheet">
</head>
<body>
<main id="main" class="main">
  <div class="pagetitle">
    <h1>Team</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
        <li class="breadcrumb-item">Edit Team</li>
      </ol>
    </nav>
  </div>
  <?php if ($this->session->flashdata('confirmacion')): ?>
		<div id="alerta-success" class="alert alert-success alert-dismissible fade show" role="alert">
			<?php echo $this->session->flashdata('confirmacion'); ?>
			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>
		<?php $this->session->set_flashdata('confirmacion',''); ?>
	<?php endif; ?>

	<script type="text/javascript">
		setTimeout(function() {
			document.getElementById('alerta-success').classList.remove('show');
		}, 3000); 
	</script>
  <!-- End Page Title -->
  <section class="section">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Edit Team</h5>
            <!-- Multi Columns Form -->
            <form class="row g-3" method="post" action="<?php echo site_url('equipos/actualizarEquipo'); ?>" enctype="multipart/form-data" id="formEquipos">
              <input type="hidden" name="id_equi" id="id_equi" value="<?php echo $equipoEditar->id_equi;?>">  
              <div class="row">
                  <div class="col-md-6">
                      <label for="nombre_equi" class="form-label"> <b>Team name:</b> </label>
                      <input type="text" name="nombre_equi" id="nombre_equi" value="<?php echo $equipoEditar->nombre_equi; ?>" class="form-control"  required>
                  </div>
                  <div class="col-md-6">
                      <label for="siglas_equi" class="form-label"> <b>Acronym:</b> </label>
                      <input type="text" name="siglas_equi" id="siglas_equi" value="<?php echo $equipoEditar->siglas_equi; ?>" class="form-control"  required>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                    <label for="fundacion_equi" class="form-label"> <b>Foundation:</b> </label>
                    <input type="number" name="fundacion_equi" id="fundacion_equi" value="<?php echo $equipoEditar->fundacion_equi; ?>" class="form-control" required>
                </div>
                <div class="col-md-4">
                  <label for="region_equi" class="form-label"><b>Region:</b></label>
                  <select name="region_equi" id="region_equi" class="form-control selectpicker "data-live-search="true">
                    <option value="Costa" <?php echo ($equipoEditar->region_equi == 'Costa') ? 'selected' : ''; ?>>Costa</option>
                    <option value="Sierra" <?php echo ($equipoEditar->region_equi == 'Sierra') ? 'selected' : ''; ?>>Sierra</option>
                    <option value="Oriente" <?php echo ($equipoEditar->region_equi == 'Oriente') ? 'selected' : ''; ?>>Oriente</option>
                    <option value="Insular" <?php echo ($equipoEditar->region_equi == 'Insular') ? 'selected' : ''; ?>>Insular</option>
                  </select>
                </div>
                <div class="col-md-4">
                    <label for="numero_titulos_equi" class="form-label white-text"> <b> Number of titles:</b> </label>
                    <input type="number" name="numero_titulos_equi" id="numero_titulos_equi" value="<?php echo $equipoEditar->numero_titulos_equi; ?>" class="form-control"  required>
                </div>
              </div>
                
              </div>
              <div class="text-center"><br>
                <button type="submit" name="button" class="btn btn-outline-warning">
                Update <i class="bx bx-refresh" ></i></button> &nbsp &nbsp
                <a type="reset" href="<?php echo site_url('equipos/index'); ?>" class="btn btn-outline-danger">
                  Cancel <i class="bx bx-message-square-x"></i>
                </a>
              </div>
            </form>
            <!-- End Multi Columns Form -->
          </div>
        </div>
      </div>
    </div>
    </main>
    <!-- jQuery -->
 <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
    <!-- Bootstrap Select JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <!-- jQuery Validation Plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <!-- jQuery Validation Additional Methods -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
    <script>
    $(document).ready(function() {
        $.validator.addMethod(
          'lettersonly',
          function(value, element) {
            return this.optional(element) || /^[a-zA-Z\s]*$/.test(value)
          },
          'Por favor, ingrese solo letras'
        )
        $.validator.addMethod(
          'yearrange',
          function(value, element) {
            var year = parseInt(value, 10);
            return this.optional(element) || (year >= 1857 && year <= 2024);
          },
          'The founding year should be between 1857 and 2024.'
        );
        $.validator.addMethod(
          'uppercaseonly',
          function(value, element) {
            return this.optional(element) || /^[A-Z]*$/.test(value);
          },
          'The acronyms should be in uppercase.'
        );
        $('#formEquipos').validate({
            rules: {
                nombre_equi: {
                    required: true,
                    lettersonly: true,
                    maxlength: 30,
                    minlength: 2,
                },
                 siglas_equi: {
                    required: true,
                    lettersonly: true,
                    uppercaseonly: true,
                    maxlength: 3,
                    minlength: 3,
                },
                fundacion_equi: {
                  required: true,
                  yearrange: true,
          
                },
                region_equi: {
                  required: true,
          
                },
                numero_titulos_equi: {
                  required: true,
				          max: 100 ,
          
                }
            },
            messages: {
                nombre_equi: {
                    required: "Please enter the team name",
                    lettersonly: 'Only letters are allowed in this field',
                    maxlength: 'The team name must be less than 30 characters',
                    minlength: 'The team name must have at least 2 characters',
                },
                 siglas_equi: {
                    required: "Please enter the team initials",
                    lettersonly: 'Only letters are allowed in this field',
                    uppercaseonly: 'The acronyms should be in uppercase.',
                    maxlength: 'The team initials must be less than 3 characters',
                    minlength: 'The team initials must have at least 3 characters',
                },
                fundacion_equi: {
                  required: "Please enter the year of foundation",
                  yearrange: 'The founding year should be between 1857 and 2024.',
                  
                },
                region_equi: {
                  required: "Please select a region",
          
                },
                numero_titulos_equi: {
                  required: "Please enter a number",
				          max: 'The number of titles should not be greater than 100.',
          
                }
            }
        });
    });
    

</script>