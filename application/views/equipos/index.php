<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Player Registration Form</title>
    <!-- Bootstrap CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- Bootstrap Select CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css" rel="stylesheet">
</head>
<body>
<main id="main" class="main">
  <div class="pagetitle">
    <h1>Team</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="<?php echo site_url('/')?>">Inicio</a></li>
        <li class="breadcrumb-item">Team</li>
      </ol>
    </nav>
  </div>
  <?php if ($this->session->flashdata('confirmacion')): ?>
		<div id="alerta-success" class="alert alert-success alert-dismissible fade show" role="alert">
			<?php echo $this->session->flashdata('confirmacion'); ?>
			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>
		<?php $this->session->set_flashdata('confirmacion',''); ?>
	<?php endif; ?>

	<script type="text/javascript">
		setTimeout(function() {
			document.getElementById('alerta-success').classList.remove('show');
		}, 3000); 
	</script>
  <!-- End Page Title -->
  <section class="section">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">Team registration</h5>
            <!-- Multi Columns Form -->
            <form class="row g-3" method="POST" action="<?php echo site_url('equipos/guardarEquipo') ?>"
              enctype="multipart/form-data" id="formEquipos">
              <div class="row">
                  <div class="col-md-6">
                      <label for="nombre_equi" class="form-label"> <b>Team name:</b> </label>
                      <input type="text" name="nombre_equi" id="nombre_equi" value="" class="form-control"  required>
                  </div>
                  <div class="col-md-6">
                      <label for="siglas_equi" class="form-label"> <b>Acronym:</b> </label>
                      <input type="text" name="siglas_equi" id="siglas_equi" value="" class="form-control"  required>
                  </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                    <label for="fundacion_equi" class="form-label"> <b>Foundation:</b> </label>
                    <input type="number" name="fundacion_equi" id="fundacion_equi" value="" class="form-control" required>
                </div>
                
                <div class="col-md-4">
                    <label for="region_equi" class="form-label"><b>Region:</b></label>
                    <select name="region_equi" id="region_equi" class="form-control selectpicker "data-live-search="true">
                      <option value="">Select a Region</option>
                      <option value="Costa">Costa</option>
                      <option value="Sierra">Sierra</option>
                      <option value="Oriente">Oriente</option>
                      <option value="Insular">Insular</option>
                    </select>
                  </div>
                  <div class="col-md-4">
                        <label for="numero_titulos_equi" class="form-label white-text"> <b> Number of titles:</b> </label>
                        <input type="number" name="numero_titulos_equi" id="numero_titulos_equi" value="" class="form-control"  required>
                    </div>
                  </div>
                
              </div>
              <div class="text-center"><br>
                <button type="submit" class="btn btn-outline-success">
                  Register <i class="bx bx-save"></i>
                </button>
                <button type="reset" class="btn btn-outline-danger">
                  Cancel <i class="bx bx-message-square-x"></i>
                </button>
              </div>
            </form>
            <!-- End Multi Columns Form -->
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <!-- Recent Ventas -->
      <div class="col-12">
        <div class="card recent-sales overflow-auto">
          <div class="card-body">
            <h5 class="card-title">List of teams</h5>
            <?php if ($listadoEquipos) : ?>
            <table class="table w-100" id="tableEquipo">
              <thead>
                <tr>
                  <th class="text-center" style="border: 1px solid #ddd;">No</th>
                  <th class="text-center" style="border: 1px solid #ddd;">TEAM NAME</th>
                  <th class="text-center" style="border: 1px solid #ddd;">TEAM ACRONYM</th>
                  <th class="text-center" style="border: 1px solid #ddd;">FUNDATION</th>
                  <th class="text-center" style="border: 1px solid #ddd;">REGION</th>
                  <th class="text-center" style="border: 1px solid #ddd;">N.TITLES</th>
                  <th class="text-center" style="border: 1px solid #ddd;">ACTIONS</th>
                </tr>
              </thead>
              <tbody>
                <?php $contador = 1; ?>
                <?php foreach ($listadoEquipos as $equipo) : ?>
                <tr>

                  <td class="text-center" style="border: 1px solid #ddd;"><?php echo $contador; ?></td>
                  <td class="text-center" style="border: 1px solid #ddd;"><?php echo $equipo->nombre_equi; ?></td>
                  <td class="text-center" style="border: 1px solid #ddd;"><?php echo $equipo->siglas_equi; ?></td>
                  <td class="text-center" style="border: 1px solid #ddd;"><?php echo $equipo->fundacion_equi; ?></td>
                  <td class="text-center" style="border: 1px solid #ddd;"><?php echo $equipo->region_equi; ?></td>
                  <td class="text-center" style="border: 1px solid #ddd;"><?php echo $equipo->numero_titulos_equi; ?></td>
                  <td class="text-center" style="border: 1px solid #ddd;">
                    <a href="<?php echo site_url('equipos/borrar/') . $equipo->id_equi; ?>"
                      class=" btn btn-outline-warning btn-eliminar" title="Borrar">
                      <i class="bi bi-trash3-fill"></i>
                    </a>
                    <a href="<?php echo site_url('equipos/editar/') . $equipo->id_equi; ?>"
                      class=" btn btn-outline-primary" title="Editar">
                      <i class="bi bi-pen"></i>
                    </a>
                  </td>
                </tr>
                <?php $contador++; ?>
                <?php endforeach; ?>
              </tbody>
            </table>
            <?php else : ?>
            <div class="alert alert-danger">
              No se encontro e$equipo registrados
            </div>
            <?php endif; ?>
          </div>
        </div>
      </div>
    </div>
    <!-- End Recent Ventas -->
    </div>
  </section>
</main>
<!-- End #main -->
 <!-- jQuery -->
 <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js"></script>
    <!-- Bootstrap Select JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>
    <!-- jQuery Validation Plugin -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/jquery.validate.min.js"></script>
    <!-- jQuery Validation Additional Methods -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.2/additional-methods.min.js"></script>
<script>
    $(document).ready(function() {
        $.validator.addMethod(
          'lettersonly',
          function(value, element) {
            return this.optional(element) || /^[a-zA-Z\s]*$/.test(value)
          },
          'Por favor, ingrese solo letras'
        )
        $.validator.addMethod(
          'yearrange',
          function(value, element) {
            var year = parseInt(value, 10);
            return this.optional(element) || (year >= 1857 && year <= 2024);
          },
          'The founding year should be between 1857 and 2024.'
        );
        $.validator.addMethod(
          'uppercaseonly',
          function(value, element) {
            return this.optional(element) || /^[A-Z]*$/.test(value);
          },
          'The acronyms should be in uppercase.'
        );
        $('#formEquipos').validate({
            rules: {
                nombre_equi: {
                    required: true,
                    lettersonly: true,
                    maxlength: 30,
                    minlength: 2,
                },
                 siglas_equi: {
                    required: true,
                    lettersonly: true,
                    uppercaseonly: true,
                    maxlength: 3,
                    minlength: 3,
                },
                fundacion_equi: {
                  required: true,
                  yearrange: true,
          
                },
                region_equi: {
                  required: true,
          
                },
                numero_titulos_equi: {
                  required: true,
				          max: 100 ,
          
                }
            },
            messages: {
                nombre_equi: {
                    required: "Please enter the team name",
                    lettersonly: 'Only letters are allowed in this field',
                    maxlength: 'The team name must be less than 30 characters',
                    minlength: 'The team name must have at least 2 characters',
                },
                 siglas_equi: {
                    required: "Please enter the team initials",
                    lettersonly: 'Only letters are allowed in this field',
                    uppercaseonly: 'The acronyms should be in uppercase.',
                    maxlength: 'The team initials must be less than 3 characters',
                    minlength: 'The team initials must have at least 3 characters',
                },
                fundacion_equi: {
                  required: "Please enter the year of foundation",
                  yearrange: 'The founding year should be between 1857 and 2024.',
                  
                },
                region_equi: {
                  required: "Please select a region",
          
                },
                numero_titulos_equi: {
                  required: "Please enter a number",
				          max: 'The number of titles should not be greater than 100.',
          
                }
            }
        });
    });
    

</script>









<script>
  $(document).ready(function() {
    // Captura el clic en el botón de eliminar
    $('.btn-eliminar').click(function(event) {
      // Previene el comportamiento predeterminado del enlace
      event.preventDefault();
      // Muestra una alerta de confirmación utilizando SweetAlert2
      Swal.fire({
        title: 'Are you sure?',
        text: "This action cannot be undone",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete',
        cancelButtonText: 'Cancel'
      }).then((result) => {
        // Si el usuario confirma, redirige al enlace de eliminación
        if (result.isConfirmed) {
          window.location.href = $(this).attr('href');
        }
      });
    });
  });
</script>
</body>
</html>