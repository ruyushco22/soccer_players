<main id="main" class="main">
	<div class="pagetitle">
		<h1>Menú</h1>
		<nav>
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="<?php echo site_url('/'); ?>">Inicio</a></li>
				<li class="breadcrumb-item active">Menú</li>
			</ol>
		</nav>
	</div>
	<!-- End Page Title -->
	<section class="section dashboard">
		<div class="row">
			<!-- Left side columns -->
			<div class="col-lg-4">
				<!-- Perfil -->
				<div class="card section profile">
					<div class="card-body profile-card pt-4 d-flex flex-column align-items-center">
						<h2 class="card-user">Usuario</h2>
						<img src="<?= base_url('assets/img/user.png') ?>" title="Perfil" class="rounded-circle" />
						<h2>Eddy Ushco</h2>
						<h3>Software Developer</h3>
						<div class="social-links mt-2">
							<a href="https://twitter.com/?lang=es" target="_blank" class="twitter"><i class="bi bi-twitter"></i></a>
							<a href="https://www.facebook.com/?locale=es_LA" target="_blank" class="facebook"><i class="bi bi-facebook"></i></a>
							<a href="https://gitlab.com/AlexHolaMundo" target="_blank" class="gitlab"><i class="bx bxl-gitlab"></i></a>
							<a href="https://www.instagram.com/" target="_blank" class="instagram"><i class="bx bxl-instagram"></i></a>
							<a href="https://github.com/AlexHolaMundo" target="_blank" class="github"><i class="bx bxl-github"></i></a>
							<a href="https://ec.linkedin.com/" target="_blank" class="linkedin"><i class="bi bi-linkedin"></i></a>
						</div>
					</div>
				</div>
				<!-- End Perfil -->
				<!-- AdminCard -->
				
				<!-- End AdminCard -->

			</div>

			<!-- End Left side columns -->
			<!-- Right side columns -->
			<div class="col-lg-8">
				<div class="row">
					<!-- Autores Card -->
					<div class="col-xxl-4 col-md-6">
						<div class="card info-card sales-card">
							<a class="card-body" href="<?php echo site_url('autores/index'); ?>">
								<h5 class="card-title">Autores</h5>
								<div class="d-flex align-items-center">
									<div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
										<i class="bi bi-person-bounding-box"></i>
									</div>
									<div class="ps-3">
										<!-- En tu plantilla HTML -->
										<h6 id="totalClientes" class="text-center">
											1
										</h6>
										<span class="text-primary small pt-1 fw-bold">Autores</span>
										<span class="text-muted small pt-2 ps-1">registrados</span>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End Autores Card -->
					<!-- Articulo Card -->
					<div class="col-xxl-4 col-md-6">
						<div class="card info-card revenue-card">
							<a class="card-body" href="<?php echo site_url('articulos/index'); ?>">
								<h5 class="card-title">Artículos</h5>
								<div class="d-flex align-items-center">
									<div class="card-icon rounded-circle d-flex align-items-center justify-content-center" style="background-color: #fffaf3">
										<i class="bx bi-newspaper" style="color: #ebc073"></i>
									</div>
									<div class="ps-3">
										<h6 id="totalProveedores" class="text-center">
											2
										</h6>
										<span class="text-dark small pt-1 fw-bold">Artículos</span>
										<span class="text-muted small pt-2 ps-1">registrados</span>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End Articulo Card -->
					<!-- Arbitraje Card -->
					<div class="col-xxl-4 col-xl-6">
						<div class="card info-card customers-card">
							<a class="card-body" href="<?php echo site_url('arbitrajes/index'); ?>">
								<h5 class="card-title">Arbitraje</h5>
								<div class="d-flex align-items-center">
									<div class="card-icon rounded-circle d-flex align-items-center justify-content-center">
										<i class="bx bi-clock-history"></i>
									</div>
									<div class="ps-3">
										<h6 id="totalCatalogos" class="text-center">
											3
										</h6>
										<span class="text-warning small pt-1 fw-bold">Arbitraje</span>
										<span class="text-muted small pt-2 ps-1">pendientes</span>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End Arbitraje Card -->
					<!-- Indexación Card -->
					<div class="col-xxl-4 col-md-6">
						<div class="card info-card sales-card">
							<a class="card-body" href="<?php echo site_url('indexaciones/index'); ?>">
								<h5 class="card-title">Indexación</h5>
								<div class="d-flex align-items-center">
									<div class="card-icon rounded-circle d-flex align-items-center justify-content-center" style="background-color: rgb(243, 232, 252)">
										<i class="bi bi-file-image" style="color: blueviolet"></i>
									</div>
									<div class="ps-3">
										<h6 id="totalPedidos" class="text-center">
											4
										</h6>
										<span class="text-dark small pt-1 fw-bold">Indexación</span>
										<span class="text-muted small pt-2 ps-1">disponibles</span>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End Indexación Card -->
					<!-- Revistas Card -->
					<div class="col-xxl-4 col-md-6">
						<div class="card info-card revenue-card">
							<a class="card-body" href="<?php echo site_url('revistas/index'); ?>">
								<h5 class="card-title">Revistas</h5>
								<div class="d-flex align-items-center">
									<div class="card-icon rounded-circle d-flex align-items-center justify-content-center" style="background-color: rgb(255, 228, 234)">
										<i class="bx bi-book" style="color: rgb(252, 84, 84)"></i>
									</div>
									<div class="ps-3">
										<h6 id="totalProductos" class="text-center">
											5
										</h6>
										<span class="text-danger small pt-1 fw-bold">Revistas</span>
										<span class="text-muted small pt-2 ps-1">registradas</span>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End Revistas Card -->
					<!-- Comite editorial Card -->
					<div class="col-xxl-4 col-xl-6">
						<div class="card info-card customers-card">
							<a class="card-body" href="<?php echo site_url('Comites_Editoriales/index'); ?>">
								<h5 class="card-title">Comite editorial</h5>
								<div class="d-flex align-items-center">
									<div class="card-icon rounded-circle d-flex align-items-center justify-content-center" style="background-color: rgb(231, 255, 229)">
										<i class="bi bi-person-raised-hand" style="color: green"></i>
									</div>
									<div class="ps-3">
										<h6 id="totalDetalles">6</h6>
										<span class="text-success small pt-1 fw-bold">Comite editorial</span>
										<span class="text-muted small pt-2 ps-1">disponibles</span>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End Comite editorial Card -->
					<!-- Comite Documento Card -->
					<div class="col-xxl-4 col-xl-6">
						<div class="card info-card customers-card">
							<a class="card-body" href="<?php echo site_url('documentos/index'); ?>">
								<h5 class="card-title">Documento</h5>
								<div class="d-flex align-items-center">
									<div class="card-icon rounded-circle d-flex align-items-center justify-content-center" style="background-color: rgb(240,254,254)">
										<i class="bx bi-journal-plus" style="color: #45e9ec"></i>
									</div>
									<div class="ps-3">
										<h6 id="totalDetalles">6</h6>
										<span class="text-info small pt-1 fw-bold">Documentos</span>
										<span class="text-muted small pt-2 ps-1">generados</span>
									</div>
								</div>
							</a>
						</div>
					</div>
					<!-- End Documento Card -->
				</div>
			</div>
		</div>
	</section>
</main>
<div style="height: 140px"></div>
