<?php
class Jugador extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    
    function insertar($datos){
        $respuesta=$this->db->insert("jugador",$datos);
        return $respuesta;
      }

    function consultarTodos(){
        $jugadores = $this->db->get("jugador");
        if ($jugadores->num_rows() > 0) {
            return $jugadores->result();
        } else {
            return false;
        }
    }
    function eliminar($id_jug){
        $this->db->where("id_jug",$id_jug);
        return $this->db->delete("jugador");
    }
    function actualizar($id_jug,$datos){
        $this->db->where("id_jug",$id_jug);
        return $this->db
                    ->update("jugador",$datos);
    }
    

      function consultarTodosPosicionesEquipos()
    {
        $this->db->select('jugador.*, posicion.nombre_pos AS nombre_pos, equipo.nombre_equi AS nombre_equi');
        $this->db->from('jugador');
        $this->db->join('posicion', 'jugador.fk_id_pos = posicion.id_pos');
        $this->db->join('equipo', 'jugador.fk_id_equi = equipo.id_equi');
        $query = $this->db->get();
        return $query->result();
    }

    function obtenerPorId($id_jug)
    {
        $this->db->select('jugador.*, posicion.nombre_pos AS nombre_pos, equipo.nombre_equi AS nombre_equi');
        $this->db->from('jugador');
        $this->db->join('posicion', 'jugador.fk_id_pos = posicion.id_pos');
        $this->db->join('equipo', 'jugador.fk_id_equi = equipo.id_equi');
        $this->db->where("id_jug", $id_jug);
        
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return $query->row(); 
        } else {
            return false;
        }
    }

    

      function consultarTodo(){
        $posiciones = $this->db->get("posicion");
        if ($posiciones->num_rows() > 0) {
            return $posiciones->result();
        } else {
            return false;
        }
      }
      function consultarTo(){
        $equipos = $this->db->get("equipo");
        if ($equipos->num_rows() > 0) {
            return $equipos->result();
        } else {
            return false;
        }
    }
  
}
?>
