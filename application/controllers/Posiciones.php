<?php

class Posiciones extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Posicion");  
    }


    public function index()
    {
        $data["listadoPosicion"] = $this->Posicion->consultarTodos();  
        $this->load->view('../views/templates1/header');
        $this->load->view("posiciones/index", $data);  
        $this->load->view('../views/templates1/footer');
    }
    public function nuevo(){
        $this->load->view('../views/templates1/header');
        $this->load->view("posiciones/nuevo");
        $this->load->view('../views/templates1/footer');
    }
    public function guardarPosicion(){

        $datosNuevoPosicion = array(
          "nombre_pos"=>$this->input->post("nombre_pos"),
          "descripcion_pos"=>$this->input->post("descripcion_pos"),
        );
        $this->Posicion->insertar($datosNuevoPosicion);
        $this->session->set_flashdata("confirmacion","Position saved successfully");
        redirect('posiciones/index');
    }
    public function borrar($id_pos){
        $this->Posicion->eliminar($id_pos);
        $this->session->set_flashdata("confirmacion","Position deleted successfully");
        redirect("posiciones/index");
    }

    
    public function editar($id_pos){
        $data["posicionEditar"]=$this->Posicion->obtenerPorId($id_pos);
        $this->load->view('../views/templates1/header');
        $this->load->view("posiciones/editar",$data);
        $this->load->view('../views/templates1/footer');
      }
      public function actualizarPosicion(){
        $id_pos=$this->input->post("id_pos");
        $datosPosicion=array(
          "nombre_pos"=>$this->input->post("nombre_pos"),
          "descripcion_pos"=>$this->input->post("descripcion_pos"),
        );
        $this->Posicion->actualizar($id_pos,$datosPosicion);
        $this->session->set_flashdata("confirmacion",
        "Position updated successfully");
        redirect('posiciones/index');
      }

  
    
}
?>
