<?php

class Equipos extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Equipo");  
    }


    public function index()
    {
        $data["listadoEquipos"] = $this->Equipo->consultarTodos();  
        $this->load->view("../views/templates1/header");
        $this->load->view("equipos/index", $data);  
        $this->load->view("../views/templates1/footer");
    }

    public function nuevo(){
        $this->load->view('../views/templates1/header');
        $this->load->view("equipos/nuevo");
        $this->load->view('../views/templates1/footer');
    }
    
    public function guardarEquipo(){

        $datosNuevoEquipo = array(
          "nombre_equi"=>$this->input->post("nombre_equi"),
          "siglas_equi"=>$this->input->post("siglas_equi"),
          "fundacion_equi"=>$this->input->post("fundacion_equi"),
          "region_equi"=>$this->input->post("region_equi"),
          "numero_titulos_equi"=>$this->input->post("numero_titulos_equi"),
          
        );
        $this->Equipo->insertar($datosNuevoEquipo);
        $this->session->set_flashdata("confirmacion","Team saved successfully");
        redirect('equipos/index');
    }
    public function borrar($id_equi){
        $this->Equipo->eliminar($id_equi);
        $this->session->set_flashdata("confirmacion","Team deleted successfully");
        redirect("equipos/index");
    }

    
    public function editar($id_equi){
        $data["equipoEditar"]=$this->Equipo->obtenerPorId($id_equi);
        $this->load->view('../views/templates1/header');
        $this->load->view("equipos/editar",$data);
        $this->load->view('../views/templates1/footer');
      }
      public function actualizarEquipo(){
        $id_equi=$this->input->post("id_equi");
        $datosEquipo=array(
          "nombre_equi"=>$this->input->post("nombre_equi"),
          "siglas_equi"=>$this->input->post("siglas_equi"),
          "fundacion_equi"=>$this->input->post("fundacion_equi"),
          "region_equi"=>$this->input->post("region_equi"),
          "numero_titulos_equi"=>$this->input->post("numero_titulos_equi"),
        );
        $this->Equipo->actualizar($id_equi,$datosEquipo);
        $this->session->set_flashdata("confirmacion",
        "Team updated successfully");
        redirect('equipos/index');
      }

  
    
}
?>
