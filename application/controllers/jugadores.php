<?php

class Jugadores extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model("Jugador");  
        $this->load->model("Posicion");
        $this->load->model("Equipo");
    }


    public function index()
    {
        $data["listadoJugadores"] = $this->Jugador->consultarTodosPosicionesEquipos();
        $data["listadoPosicion"] = $this->Jugador->consultarTodo();  
        $data["listadoEquipos"] = $this->Jugador->consultarTo(); 
        $this->load->view('../views/templates1/header');
        $this->load->view("jugadores/index", $data);  
        $this->load->view('../views/templates1/footer');
    }

    
    public function guardarJugador() {
        // Obtener datos del formulario y almacenar en un array
        $datosNuevoJugador = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"),
            "fk_id_pos" => $this->input->post("fk_id_pos"),
            "fk_id_equi" => $this->input->post("fk_id_equi"),
        );
    
        // Cargar el modelo Jugador para insertar los datos
        $this->load->model('Jugador');
        
        // Llamar al método insertar del modelo para guardar el nuevo jugador
        $insertado = $this->Jugador->insertar($datosNuevoJugador);
    
        if ($insertado) {
            // Si se insertó correctamente, configurar mensaje de confirmación
            $this->session->set_flashdata("confirmacion", "Player saved successfully");
        } else {
            // Manejar el caso donde la inserción falló (opcional)
            $this->session->set_flashdata("error", "Failed to save player");
        }
    
        // Redirigir al controlador de jugadores (o a donde desees)
        redirect('jugadores/index');
    }
    

    public function borrar($id_jug){
        $this->Jugador->eliminar($id_jug);
        $this->session->set_flashdata("confirmacion","Player deleted successfully");
        redirect("jugadores/index");
    }

    public function grafico()
    {
        $this->load->view('../views/templates1/header');
        $this->load->view("jugadores/grafico");  
        $this->load->view('../views/templates1/footer');
    }
    public function editar($id_jug){
        $data["JugadorEditar"] = $this->Jugador->obtenerPorId($id_jug);
        $data["listadoPosicion"] = $this->Jugador->consultarTodo();
        $data["listadoEquipos"] = $this->Jugador->consultarTo();
        
        $this->load->view('../views/templates1/header');
        $this->load->view("jugadores/editar", $data);
        $this->load->view('../views/templates1/footer');
    }
  
    public function actualizarJugador(){
        $id_jug = $this->input->post("id_jug");
        
        $datosJugado = array(
            "apellido_jug" => $this->input->post("apellido_jug"),
            "nombre_jug" => $this->input->post("nombre_jug"),
            "estatura_jug" => $this->input->post("estatura_jug"),
            "salario_jug" => $this->input->post("salario_jug"),
            "estado_jug" => $this->input->post("estado_jug"),
            "fk_id_pos" => $this->input->post("fk_id_pos"),
            "fk_id_equi" => $this->input->post("fk_id_equi")
        );
        
        $this->Jugador->actualizar($id_jug, $datosJugado);
        
        $this->session->set_flashdata("confirmacion", "Player updated successfully");
        redirect('jugadores/index');
    }
  



}
?>
